package mis49m.com.httpurlconnectionlab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import mis49m.com.httpurlconnectionlab.util.WebServiceUtil;

public class MainActivity extends AppCompatActivity {

    TextView tvContent;
    EditText etUrl;
    ImageView imageView;
    Button btnImage, btnUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        tvContent = (TextView) findViewById(R.id.tv_content);
        etUrl = (EditText) findViewById(R.id.et_url);
        imageView = (ImageView) findViewById(R.id.image);
        btnImage = (Button) findViewById(R.id.btn_image);
        btnUrl = (Button) findViewById(R.id.btn_url);

        etUrl.setText("http://umitopacan.com/mis49m/data1.json");

        btnUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               
            }
        });

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
